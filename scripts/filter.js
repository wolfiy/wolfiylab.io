/**
 * Pills and buttons used to filter the projects categories.
 */
var pills = document.getElementById("pills");
var btns = pills.getElementsByClassName("btn");

/**
 * Show all by default.
 */
filterProjects('all')

/**
 * Filters the projects.
 * Heavily inspired by w3school's example.
 * See https://www.w3schools.com/howto/howto_js_portfolio_filter.asp
 */
 function filterProjects(c) {
    var x, i;
    x = document.getElementsByClassName("projects-container");
    if (c == "all") c = "";
    for (i = 0; i < x.length; ++i) {
        x[i].classList.remove("show");
        if (x[i].className.indexOf(c) > -1) x[i].classList.add("show");
    }
}

/**
 * Loops through all buttons to add the 'active' class to the clicked button.
 */
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
} 