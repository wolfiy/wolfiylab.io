/**
 * Allows theme switching using local storage to remember selection.
 */

let lightMode = localStorage.getItem('lightMode');
const lightModeToggle = document.querySelector('#light-mode-toggle');

/**
 * Enables the light mode.
 */
const enable = () => {
	document.body.classList.add('lightmode');
	localStorage.setItem('lightMode', 'enabled');
}

/**
 * Disables the lightmode.
 */
const disable = () => {
	document.body.classList.remove('lightmode');
	localStorage.setItem('lightMode', null);
}

/**
 * Actual switch.
 */
if (lightMode === 'enabled') enable();
lightModeToggle.addEventListener('click', () => {
	lightMode = localStorage.getItem('lightMode');
	lightMode == 'enabled' ? disable() : enable();
});