/**
 * Offset between the current position and the top of the page.
 */
var oldScrollPos = window.pageYOffset;

/**
 * Navbar's height.
 */
var navHeight = "-68px";

/**
 * Runs all functions related to the navbar on mouse scroll.
 */
window.onscroll = function() {
    opacityCheck();
    hideShowNavbar();
}

/**
 * Shows or hide the navbar depending on whether the user scrolled up or down.
 */
function hideShowNavbar() {
    var navbar = document.getElementById("navbar");
    var newScrollPos = window.pageYOffset;

    oldScrollPos > newScrollPos ? navbar.style.top = "0" : navbar.style.top = navHeight;
    oldScrollPos = newScrollPos;
}

/**
 * Changes the opacity of the navbar depening on its position. On the top, it should not be solid.
 */
function opacityCheck() {
    var nav = document.getElementById("navbar");
    var topThreshold = nav.offsetTop;
    window.pageYOffset > topThreshold + 10 ? nav.classList.add("solid") : nav.classList.remove("solid");
}